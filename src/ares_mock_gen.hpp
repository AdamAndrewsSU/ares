/*+
    ARES/HADES/BORG Package -- -- ./src/ares_mock_gen.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __ARES_MOCK_GEN_HPP
#define __ARES_MOCK_GEN_HPP

namespace LibLSS {

    template<typename PTree>
    void prepareMockData(PTree& ptree, MPI_Communication *comm, MarkovState& state,
                        CosmologicalParameters& cosmo_params,
                        SamplerBundle& bundle
                        ) {
        ConsoleContext<LOG_INFO_SINGLE> ctx("prepareMockData");
        using boost::format;
     
        createCosmologicalPowerSpectrum(state, cosmo_params);

        bundle.sel_updater.sample(state);
        bundle.sampler_s.setMockGeneration(true);
        bundle.sampler_catalog_projector.setMockGeneration(true);
        
        // Generate the tau
        bundle.sampler_catalog_projector.sample(state);
        bundle.sampler_s.sample(state);
        // Generate the data
        bundle.sampler_catalog_projector.sample(state);
        
        bundle.sampler_s.setMockGeneration(false);
        bundle.sampler_t.setMockGeneration(false);
        bundle.sampler_catalog_projector.setMockGeneration(false);

        {
            std::string fname = str(format("mock_data.h5_tmp_%d") % comm->rank());
            H5::H5File f(fname.c_str(), H5F_ACC_TRUNC);
            state.mpiSaveState(f, comm, false);
            
            if (comm->rank() == 0) {
                rename(fname.c_str(), "mock_data.h5");
            } else {
                unlink(fname.c_str());
            }
        }
            
        state.get<ArrayType>("s_field")->eigen().fill(0);
        state.get<ArrayType>("x_field")->eigen().fill(0);
    }
}

#endif
