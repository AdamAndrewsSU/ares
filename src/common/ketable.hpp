/*+
    ARES/HADES/BORG Package -- -- ./src/common/ketable.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_KETABLE_HPP
#define __LIBLSS_KETABLE_HPP

#include <CosmoTool/interpolate.hpp>
#include <string>
#include <H5Cpp.h>

namespace LibLSS {
    
    class KETableCorrection {
    protected:
        bool no_correction;
        CosmoTool::Interpolate data;
    public:
        KETableCorrection() { no_correction = true; }
        KETableCorrection(const std::string& fname)
            : data(CosmoTool::buildInterpolateFromFile(fname.c_str())), no_correction(false) {
        }
        
        double getZCorrection(double z) {
            if (no_correction)
                return 0;
            else
                return data.compute(z);
        }
        
        void save(H5::CommonFG& fg) {
        }
        
        void restore(H5::CommonFG& fg) {
        }
    };
}

#endif