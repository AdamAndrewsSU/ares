/*+
    ARES/HADES/BORG Package -- -- ./src/common/preparation.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2018)
       Minh Nguyen <minh@mpa-garching.mpg.de> (2017)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_ARES_PREPARATION_HPP
#define __LIBLSS_ARES_PREPARATION_HPP

#include <functional>
#include "libLSS/tools/console.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include "libLSS/tools/ptree_translators.hpp"
#include <boost/algorithm/string.hpp>
#include "libLSS/data/spectro_gals.hpp"
#include "libLSS/data/galaxies.hpp"
#include "libLSS/data/survey_load_txt.hpp"
#include "libLSS/data/survey_load_bin.hpp"
#include "libLSS/data/projection.hpp"
#include "libLSS/data/linear_selection.hpp"
#include "libLSS/data/window3d.hpp"
#include "libLSS/data/window3d_post.hpp"
#include "libLSS/data/schechter_completeness.hpp"
#include "survey_cutters.hpp"
#include "piecewise_selection.hpp"
#include "ketable.hpp"
#include <CosmoTool/interpolate.hpp>
#include "libLSS/tools/ptree_vectors.hpp"
#include "libLSS/tools/fused_array.hpp"
#include "libLSS/tools/fused_assign.hpp"
#include "libLSS/tools/fusewrapper.hpp"
#include "libLSS/physics/modified_ngp.hpp"
#include "libLSS/physics/classic_cic.hpp"

#include "preparation_types.hpp"
#include "preparation_tools.hpp"

namespace LibLSS_prepare {

    static void initSchechterVariables(MarkovState& state, ptree& params, int cat_idx)
    {
        state.newElement(format("galaxy_selection_info_%d") % cat_idx, new InfoSampleSelection());
        state.newElement(format("galaxy_schechter_%d") % cat_idx, new InfoSchechter());
    }

    static void initializeGalaxySurveyCatalog(MarkovState& state, ptree& main_params, int cat_idx)
    {
        using PrepareDetail::ArrayDimension;
        size_t N[3];
            N[0] = static_cast<SLong&>(state["N0"]);
            N[1] = static_cast<SLong&>(state["N1"]);
            N[2] = static_cast<SLong&>(state["N2"]);
        size_t
            localN0 = static_cast<SLong&>(state["localN0"]),
            startN0 = static_cast<SLong&>(state["startN0"]);
        Console& cons = Console::instance();
        ptree& params = main_params.get_child(get_catalog_group_name(cat_idx));

        GalaxyElement *survey = new GalaxyElement();
        survey->obj = new GalaxySurveyType();
        // Add a catalog in the state structure
        state.newElement(format("galaxy_catalog_%d") % cat_idx, survey);
        // Add its linear bias in the MCMC structure
        SDouble *nmean = new SDouble();
        ArrayType1d *bias = new ArrayType1d(boost::extents[0]);
        state.newElement(format("galaxy_bias_%d") % cat_idx, bias, true);
        state.newElement(format("galaxy_nmean_%d") % cat_idx, nmean, true);
        bias->setAutoResize(true);

        cons.print<LOG_DEBUG>(format("Allocating selection grid for catalog %d") % cat_idx);
        SelArrayType *sel_grid= new SelArrayType(boost::extents[range(startN0,startN0+localN0)][N[1]][N[2]]);
        cons.print<LOG_DEBUG>(format("Allocating projection grid for catalog %d") % cat_idx);
        ArrayType *data_grid = new ArrayType(boost::extents[range(startN0,startN0+localN0)][N[1]][N[2]]);
        SBool *biasRef = new SBool();

        data_grid->setRealDims(ArrayDimension(N[0], N[1], N[2]));
        sel_grid->setRealDims(ArrayDimension(N[0], N[1], N[2]));
        state.newElement(format("galaxy_bias_ref_%d") % cat_idx, biasRef);
        state.newElement(format("galaxy_sel_window_%d") % cat_idx, sel_grid);
        state.newElement(format("galaxy_data_%d") % cat_idx, data_grid);

        KECorrectionStateElement *ke_obj = new KECorrectionStateElement();
        KETableCorrection *ke;
        state.newElement(format("galaxy_kecorrection_%d") % cat_idx, ke_obj);
        if (boost::optional<string> ketable = params.get_optional<string>("ke_correction")) {
          cons.print<LOG_INFO_SINGLE>("Applying correction from file " + *ketable);
          ke = new KETableCorrection(*ketable);
        } else {
          ke = new KETableCorrection();
        }
        ke_obj->obj = ke;

        string radtype = to_lower_copy(params.get<string>("radial_selection"));
        if (radtype == "schechter") {
            cons.print<LOG_DEBUG>("initializing Schechter radial selection");
            initSchechterVariables(state, params, cat_idx);
        } else if (radtype == "piecewise") {
            cons.print<LOG_DEBUG>("initializing Piecewise selection");
            state.newElement(format("galaxy_selection_info_%d") % cat_idx, new InfoSampleSelection());
        } else if (radtype == "file") {
            state.newElement(format("galaxy_selection_info_%d") % cat_idx, new InfoSampleSelection());
        }
    }

    static void buildSchechterSelectionForSurvey(GalaxyElement *survey, MarkovState& state, ptree& params,
            int cat_idx, CosmologicalParameters& cosmo_params, SurveyPreparer& preparer,
            CorrectionFunction zcorr = details::nullCorrection)
    {
        ConsoleContext<LOG_INFO_SINGLE> ctx("schechter completeness for survey");
        Cosmology cosmo(cosmo_params);
        int Nsample = params.get<int>("schechter_sampling_rate");
        double Dmax = params.get<double>("schechter_dmax");
        boost::multi_array<double, 1> completeness(boost::extents[Nsample]);
        namespace ph = std::placeholders;

        GalaxySampleSelection& infosel = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;
        SchechterParameters& infolum = state.get<InfoSchechter>(format("galaxy_schechter_%d") % cat_idx)->value;
        KETableCorrection& ke = state.get<KECorrectionStateElement>(format("galaxy_kecorrection_%d") % cat_idx)->get();

        infosel.bright_apparent_magnitude_cut = params.get<double>("galaxy_bright_apparent_magnitude_cut");
        infosel.faint_apparent_magnitude_cut = params.get<double>("galaxy_faint_apparent_magnitude_cut");
        infosel.bright_absolute_magnitude_cut = params.get<double>("galaxy_bright_absolute_magnitude_cut");
        infosel.faint_absolute_magnitude_cut = params.get<double>("galaxy_faint_absolute_magnitude_cut");
        infosel.zmin = params.get<double>("zmin", 0);
        infosel.zmax = params.get<double>("zmax", 100000);
        infosel.projection = state.getScalar<ProjectionDataModel>("projection_model");

        infolum.Mstar = params.get<double>("schechter_mstar");
        infolum.alpha = params.get<double>("schechter_alpha");

        buildCompletenessFromSchechterFunction(cosmo, infosel,
          infolum, completeness, Dmax,
          boost::bind(&KETableCorrection::getZCorrection, &ke, _1));

        survey->get().selection().setArray(completeness, Dmax);
        survey->get().selection().setMinMaxDistances(0, Dmax);

        switch (infosel.projection) {
            case NGP_PROJECTION:
              preparer = std::bind<size_t>(
                  galaxySurveyToGridGeneric<
                    ModifiedNGP<double, NGPGrid::NGP, true>,
                    GalaxySurveyType,
                    ArrayType::ArrayType,
                    double *,
                    size_t *,
                    RedshiftMagnitudeCutter<GalaxySurveyType>
                  >,
                  ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
                  RedshiftMagnitudeCutter<GalaxySurveyType>(infosel, survey->obj),
                  std::function<void()>()
              );
              break;
            case LUMINOSITY_CIC_PROJECTION: {
              preparer = std::bind<size_t>(
                galaxySurveyToGridGeneric<
                  ClassicCloudInCell<double, true>,
                  GalaxySurveyType,
                  ArrayType::ArrayType,
                  double *,
                  size_t *,
                  RedshiftMagnitudeCutter<GalaxySurveyType>
                >,
                ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
                RedshiftMagnitudeCutter<GalaxySurveyType>(infosel, survey->obj),
                std::function<void()>(boost::bind(&GalaxySurveyType::useLuminosityAsWeight, survey->obj))
              );
              break;
            }
          default:
              error_helper<ErrorParams>("Unsupported data projection");
              break;
        }
    }

    static void buildPiecewiseSelection(GalaxySurveyType& survey, MarkovState& state, ptree& params,
            int cat_idx, CosmologicalParameters& cosmo_params, SurveyPreparer& preparer)
    {
        namespace ph = std::placeholders;
        GalaxySampleSelection& infosel = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;

        infosel.bright_apparent_magnitude_cut = params.get<double>("galaxy_bright_apparent_magnitude_cut");
        infosel.faint_apparent_magnitude_cut = params.get<double>("galaxy_faint_apparent_magnitude_cut");
        infosel.bright_absolute_magnitude_cut = params.get<double>("galaxy_bright_absolute_magnitude_cut");
        infosel.faint_absolute_magnitude_cut = params.get<double>("galaxy_faint_absolute_magnitude_cut");
        infosel.zmin = params.get<double>("zmin");
        infosel.zmax = params.get<double>("zmax");
        infosel.projection = state.getScalar<ProjectionDataModel>("projection_model");

        infosel.selector = cutterFunction(RedshiftMagnitudeCutter<GalaxySurveyType>(infosel, &survey));

        switch (state.getScalar<ProjectionDataModel>("projection_model")) {
            case NGP_PROJECTION:
              preparer = std::bind<size_t>(
                    galaxySurveyToGridGeneric<
                      ModifiedNGP<double,NGPGrid::NGP, true>,
                      GalaxySurveyType,
                      ArrayType::ArrayType,
                      double *,
                      size_t *,
                      RedshiftMagnitudeCutter<GalaxySurveyType>,
                      std::function<void()>
                    >, ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
                    RedshiftMagnitudeCutter<GalaxySurveyType>(infosel, &survey),
                    std::function<void()>()
                  );
              break;

            // If the projection is LUMINOSITY_CIC, then we have to use a CIC kernel
            case LUMINOSITY_CIC_PROJECTION:
              preparer = std::bind<size_t>(
                  galaxySurveyToGridGeneric<
                    ClassicCloudInCell<double, true>,
                    GalaxySurveyType,
                    ArrayType::ArrayType,
                    double *,
                    size_t *,
                    RedshiftMagnitudeCutter<GalaxySurveyType>
                  >, ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
                  RedshiftMagnitudeCutter<GalaxySurveyType>(infosel, &survey),
                  std::function<void()>(std::bind(&GalaxySurveyType::useLuminosityAsWeight, &survey))
                );
            break;
          }

    }

    static void buildFileSelectionForSurvey(GalaxyElement* survey, MarkovState& state, ptree& params,
            int cat_idx, CosmologicalParameters& cosmo_params, SurveyPreparer& preparer)
    {
        namespace ph = std::placeholders;
        ConsoleContext<LOG_INFO_SINGLE> ctx("file completeness for survey");
        string radial_map = params.get<string>("radial_file");

        ctx.print(format("Load radial selection function '%s'") % radial_map);
        survey->get().selection().loadRadial( radial_map );

        GalaxySampleSelection& selection = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;

        selection.zmin = params.get<double>("zmin", 0);
        selection.zmax = params.get<double>("zmax", 100000);
        selection.projection = state.getScalar<ProjectionDataModel>("projection_model");


        if (!details::safe_get(params, "galaxy_bright_apparent_magnitude_cut", selection.bright_apparent_magnitude_cut) ||
            !details::safe_get(params, "galaxy_faint_apparent_magnitude_cut", selection.faint_apparent_magnitude_cut) ||
            !details::safe_get(params, "galaxy_bright_absolute_magnitude_cut", selection.bright_absolute_magnitude_cut) ||
            !details::safe_get(params, "galaxy_faint_absolute_magnitude_cut", selection.faint_absolute_magnitude_cut)
           ) {
           ctx.print("No information on luminosity cuts. Taking all galaxies inside a d range");

           bool no_cut_catalog = true;
           if (!details::safe_get(params, "no_cut_catalog", no_cut_catalog) && no_cut_catalog)
             error_helper<ErrorParams>(boost::format("You have to confirm not to cut properly your catalog %d") % cat_idx);

           try {
             selection.dmin = params.get<double>("file_dmin", 0);
             selection.dmax = params.get<double>("file_dmax", 1e6); // No cut effectively
             survey->get().selection().setMinMaxDistances( selection.dmin, selection.dmax);
           } catch(const std::runtime_error&) {
             error_helper<ErrorParams>("Incorrect/Unknown file_dmin or file_dmax in configuration file");
           }
           preparer = std::bind<size_t>(
              galaxySurveyToGridGeneric<
                ModifiedNGP<double,NGPGrid::NGP, true>,
                GalaxySurveyType,
                ArrayType::ArrayType,
                double *,
                size_t *,
                DistanceCutter<GalaxySurveyType>
              >, ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
              DistanceCutter<GalaxySurveyType>(selection, survey->obj),
              std::function<void()>()
           );
        } else {
          Cosmology cosmo(cosmo_params);

          selection.dmin = cosmo.com2comph(cosmo.a2com(cosmo.z2a(selection.zmin)));
          selection.dmax = cosmo.com2comph(cosmo.a2com(cosmo.z2a(selection.zmax)));
          survey->get().selection().setMinMaxDistances( selection.dmin, selection.dmax);

          preparer = std::bind<size_t>(
              galaxySurveyToGridGeneric<
                ModifiedNGP<double,NGPGrid::NGP, true>,
                GalaxySurveyType,
                ArrayType::ArrayType,
                double*,
                size_t *,
                RedshiftMagnitudeCutter<GalaxySurveyType>
              >, ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
              RedshiftMagnitudeCutter<GalaxySurveyType>(selection, survey->obj),
              std::function<void()>()
          );
        }
    }

    static void loadGalaxySurveyCatalog(MarkovState& state, ptree& main_params, int cat_idx, CosmologicalParameters& cosmo_params,
                            SurveyPreparer& preparer)
    {
        ConsoleContext<LOG_INFO_SINGLE> ctx(str(format("loadGalaxySurveyCatalog(%d)") % cat_idx));
        GalaxyElement *survey = state.get<GalaxyElement>(format("galaxy_catalog_%d") % cat_idx);
        ptree& params = main_params.get_child(get_catalog_group_name(cat_idx));
        std::string data_format = params.get<std::string>("dataformat", "TXT");

        if (data_format == "TXT") {
          loadGalaxySurveyFromText(
                  params.get<string>("datafile"),
                  survey->get()
          );
	} else if (data_format == "HDF5") {
           loadCatalogFromHDF5(
                  params.get<string>("datafile"),
		  params.get<string>("datakey"),
                  survey->get()
           );
	} else {
          error_helper<ErrorParams>(format("data_format has value %s, which is not recognized") % data_format);
	}

        state.get<SBool>(format("galaxy_bias_ref_%d") % cat_idx)->value =
            params.get<bool>("refbias");

        ArrayType1d::ArrayType& gbias = *(state.get<ArrayType1d>(format("galaxy_bias_%d") % cat_idx)->array);
        if (boost::optional<std::string> bvalue = params.get_optional<std::string>("bias")) {
            auto bias_double = string_as_vector<double>(*bvalue);
            gbias.resize(boost::extents[bias_double.size()]);
            std::copy(bias_double.begin(), bias_double.end(), gbias.begin());
            string fmt_str = "Set the bias to [";
            for (int i = 0; i < gbias.size()-1; i++)
              fmt_str += "%lg,";
            fmt_str += "%lg]";
            auto fmt = boost::format(fmt_str);
            for (int i = 0; i < gbias.size()-1; i++)
              fmt = fmt % gbias[i];
            fmt = fmt % gbias[gbias.size()-1];
            ctx.print(fmt);
        } else {
            ctx.print("No initial bias value set, use bias=1");
            gbias.resize(boost::extents[1]);
            gbias[0] = 1;
        }

        double& nmean = state.get<SDouble>(format("galaxy_nmean_%d") % cat_idx)->value;
        if (boost::optional<double> nvalue = params.get_optional<double>("nmean")) {
            nmean = *nvalue;
        } else {
            ctx.print("No initial mean density value set, use nmean=1");
            nmean = 1;
        }

        string compl_map = params.get<string>("maskdata");
        ctx.print(format("Load sky completeness map '%s'") % compl_map);
        survey->get().selection().loadSky( compl_map, params.get<double>("sky_threshold", 0));

        string radtype = to_lower_copy(params.get<string>("radial_selection"));

        GalaxySelectionType& gsel_type = state.newScalar<GalaxySelectionType>(format("galaxy_selection_type_%d") % cat_idx, GALAXY_SELECTION_FILE)->value;

        if (radtype == "file") {
            buildFileSelectionForSurvey(survey, state, params, cat_idx, cosmo_params, preparer);

            gsel_type = GALAXY_SELECTION_FILE;
        } else if (radtype == "schechter") {

            buildSchechterSelectionForSurvey(survey, state, params, cat_idx, cosmo_params, preparer);
            gsel_type = GALAXY_SELECTION_SCHECHTER;
        } else if (radtype == "piecewise") {
            gsel_type = GALAXY_SELECTION_PIECEWISE;
            buildPiecewiseSelection(*survey->obj, state, params, cat_idx, cosmo_params, preparer);
        } else {
            error_helper<ErrorParams>(format("radtype has value %s, which is not recognized") % radtype);
        }
    }

    template<typename ptree>
    void prepareData(MPI_Communication *comm, MarkovState& state, int cat_idx, CosmologicalParameters& cosmo_params,
                     const SurveyPreparer& preparer, ptree& main_params) {
        using CosmoTool::InvalidRangeException;

        size_t N[3];
            N[0] = static_cast<SLong&>(state["N0"]);
            N[1] = static_cast<SLong&>(state["N1"]);
            N[2] = static_cast<SLong&>(state["N2"]);
        size_t
            N2_HC = static_cast<SLong&>(state["N2_HC"]),
            localN0 = static_cast<SLong&>(state["localN0"]),
            startN0 = static_cast<SLong&>(state["startN0"]);
        double L[3], delta[3], corner[3];
        ConsoleContext<LOG_INFO_SINGLE> ctx("data preparation");
        Cosmology cosmo(cosmo_params);
        ptree& sys_params = main_params.get_child("system");
        ptree& g_params = main_params.get_child(get_catalog_group_name(cat_idx));

        ctx.print(format("Project data to density field grid (catalog %d)") % cat_idx);

        L[0] = static_cast<SDouble&>(state["L0"]);
        L[1] = static_cast<SDouble&>(state["L1"]);
        L[2] = static_cast<SDouble&>(state["L2"]);

        corner[0] = static_cast<SDouble&>(state["corner0"]);
        corner[1] = static_cast<SDouble&>(state["corner1"]);
        corner[2] = static_cast<SDouble&>(state["corner2"]);

        GalaxySurveyType& survey = state.get<GalaxyElement>(str(format("galaxy_catalog_%d") % cat_idx))->get();
        KECorrectionStateElement *ke = state.get<KECorrectionStateElement>(format("galaxy_kecorrection_%d") % cat_idx);
        try {
            survey.updateComovingDistance(cosmo, boost::bind(&KETableCorrection::getZCorrection, ke->obj, _1));
        } catch (const InvalidRangeException& e) {
            error_helper<ErrorBadState>("Invalid range access in KE correction interpolation");
        }
        ArrayType *data_grid = state.get<ArrayType>(format("galaxy_data_%d") % cat_idx);

        delta[0] = L[0] / N[0];
        delta[1] = L[1] / N[1];
        delta[2] = L[2] / N[2];

        size_t numGals = preparer(survey, *(data_grid->array), N, corner, L, delta);
        comm->all_reduce_t(MPI_IN_PLACE, &numGals, 1, MPI_SUM);
        if (numGals == 0) {
          error_helper<ErrorBadState>(format("No galaxy at all in catalog %d") % cat_idx);
        }

        GalaxySelectionType& gsel_type = state.getScalar<GalaxySelectionType>(format("galaxy_selection_type_%d") % cat_idx);
        GalaxySampleSelection& infosel = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;
        if (gsel_type == GALAXY_SELECTION_PIECEWISE) {
           computeEmpiricalSelection(&survey,
              infosel,
              g_params.template get<double>("piecewise_dmin"),
              g_params.template get<double>("piecewise_dmax"),
              g_params.template get<int>("piecewise_Nbins"));
        }

        SelArrayType *sel_grid= state.get<SelArrayType>(format("galaxy_sel_window_%d") % cat_idx);

        PrepareDetail::compute_window(sys_params, comm, survey.selection(), state, *sel_grid->array, true);

        if (infosel.projection == LUMINOSITY_CIC_PROJECTION) {
          convolve_selection_cic(comm, *sel_grid->array, N);
        }

        auto fsel = fwrap(*sel_grid->array);
        fsel = mask(fsel > 0.05, fsel, fwrap(fsel.fautowrap(0)));

        PrepareDetail::cleanup_data(*data_grid->array, *sel_grid->array);

        // Free memory at the expense of information in logs
        survey.selection().clearSky();
    }


}

#endif
