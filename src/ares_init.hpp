/*+
    ARES/HADES/BORG Package -- -- ./src/ares_init.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_ARES_COMMON_INIT_HPP
#define __LIBLSS_ARES_COMMON_INIT_HPP

#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/tools/console.hpp"
#include "libLSS/samplers/core/main_loop.hpp"
#include "libLSS/mcmc/state_element.hpp"
#include "common/foreground.hpp"

namespace LibLSS_prepare {

  template<typename PTree>
  static bool check_is_simulation(PTree& params) {
    return params.get_child("run").template get<bool>("SIMULATION", false);
  }

  template<typename PTree>
  static void sampler_init_data(
        MPI_Communication *mpi_world,
        MarkovState& state, PTree& params)
  {
    long Ncat = state.getSyncScalar<long>("NCAT");
    // ==================
    if (check_is_simulation(params)) {
      for (int i = 0; i < Ncat; i++)
           initializeHaloSimulationCatalog(
                   state,
                   params, i);
    } else {
      for (int i = 0; i < Ncat; i++)
           initializeGalaxySurveyCatalog(
                   state,
                   params, i);
    }

  }

  template<typename PTree>
  static void sampler_load_data(
        MPI_Communication *mpi_world,
        MarkovState& state, PTree& params, MainLoop& loop)
  {
    long Ncat = state.getSyncScalar<long>("NCAT");
    CosmologicalParameters& cosmo = state.getScalar<CosmologicalParameters>("cosmology");

    if (check_is_simulation(params)) {
      for (int i = 0; i < Ncat; i++) {
        SimPreparer preparer;

        loadHaloSimulationCatalog(
          state, params,
          i, cosmo, preparer);
        prepareHaloSimulationData(mpi_world, state, i, cosmo, preparer, params);
      }
    } else {
      for (int i = 0; i < Ncat; i++) {
        SurveyPreparer preparer;

        loadGalaxySurveyCatalog(
          state, params,
          i, cosmo, preparer);
        prepareData(mpi_world, state, i, cosmo, preparer, params);
      }
    }

    // Load&Build foregrounds
    loadForegrounds(mpi_world, loop, params);

  }
}

#endif
