#+
#   ARES/HADES/BORG Package -- -- ./build_tools/gen_splash.py
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
import sys
import re

prev_line=None
with open(sys.argv[1], mode="r") as f_in, open(sys.argv[2], mode="w") as f_out:
  for line in f_in:
    if prev_line is not None:
      f_out.write('"' + prev_line + '",\n')
    line = re.sub(r'\\', r'\\\\', line)
    line = re.sub(r'"', r'\"', line)
    prev_line = line[:-1]
  f_out.write('"' + prev_line + '"\n')
