OPTION(BUILD_DOCUMENTATION OFF)

IF (BUILD_DOCUMENTATION)
include(${CMAKE_SOURCE_DIR}/cmake_modules/UseLATEX.cmake)

SET(main_tex manual.tex)
SET(chapters_tex 
  classicthesis-config.tex
  introduction.tex
  installation.tex
  outputs.tex
  running.tex
  bibliography.tex
  titlepage.tex
  2mpp.ini
)

add_latex_document(${main_tex} 
    INPUTS ${chapters_tex}
    DEPENDS ${chapters_tex}
    BIBFILES Bibliography.bib 
    FORCE_PDF)

ENDIF(BUILD_DOCUMENTATION)
