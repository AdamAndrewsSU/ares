/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/ptree_translators.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_PTREE_TRANSLATORS_HPP
#define __LIBLSS_PTREE_TRANSLATORS_HPP

#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include "libLSS/tools/errors.hpp"

namespace LibLSS {

  namespace PTreeTools {

    // Custom translator for bool (only supports std::string)
    struct BoolTranslator {
      typedef std::string internal_type;
      typedef bool        external_type;

      // Converts a string to bool
      boost::optional<external_type> get_value(const internal_type& str) {

	      Console::instance().print<LOG_VERBOSE>("Translating " + str);

        if (!str.empty()) {
          using boost::algorithm::iequals;

          if (iequals(str, "true") || iequals(str, "yes") || str == "1")
            return boost::optional<external_type>(true);
          else if (iequals(str, "false") || iequals(str, "no") || str == "0")
            return boost::optional<external_type>(false);
          else {
            Console::instance().print<LOG_ERROR>("Error while translating to boolean.");
            throw ErrorBadCast("String '" + str + "' cannot be cast to boolean");
          }
        } else {
	         Console::instance().print<LOG_VERBOSE>("  =+= String empty");
           return boost::optional<external_type>(boost::none);
	      }
      }

      // Converts a bool to string
      boost::optional<internal_type> put_value(const external_type& b) {
        return boost::optional<internal_type>(b ? "true" : "false");
      }
    };

  };
};

/*  Specialize translator_between so that it uses our custom translator for
    bool value types. Specialization must be in boost::property_tree
    namespace. */
namespace boost {
  namespace property_tree {

    template<typename Ch, typename Traits, typename Alloc>
    struct translator_between<std::basic_string< Ch, Traits, Alloc >, bool> {
        typedef LibLSS::PTreeTools::BoolTranslator type;
    };

  } // namespace property_tree

} // namespace boost

#endif
