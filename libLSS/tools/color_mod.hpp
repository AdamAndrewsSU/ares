/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/color_mod.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_COLOR_MOD_HPP
#define __LIBLSS_COLOR_MOD_HPP

#include <string>
#include <boost/format.hpp>

namespace LibLSS {

    namespace Color {
    
        enum ColorValue {
            BLACK = 0,
            RED = 1,
            GREEN = 2,
            YELLOW = 3,
            BLUE = 4,
            MAGENTA = 5,
            CYAN = 6,
            WHITE = 7
        };
        
        enum ColorIntensity {
            NORMAL = 0,
            BRIGHT = 1
        };
        
        inline std::string fg(ColorValue c, const std::string& text, ColorIntensity i = NORMAL, bool is_console = true) {
            if (is_console)
                return boost::str(boost::format("\x1b[%d;%dm%s\x1b[39;0m") % ((int)c + 30) % (int)i % text);
            else
                return text;
        }

        inline std::string bg(ColorValue c, const std::string& text, ColorIntensity i = NORMAL, bool is_console = true) {
            if (is_console)
                return boost::str(boost::format("\x1b[%d;%dm%s\x1b[49;0m") % ((int)c + 40) % (int)i % text);
            else
                return text;
        }
    
    }

}

#endif
