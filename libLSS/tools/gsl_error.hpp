/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/gsl_error.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_GSL_ERROR_HPP
#define __LIBLSS_GSL_ERROR_HPP

#include "libLSS/tools/static_auto.hpp"

AUTO_REGISTRATOR_DECL(GSL_Error);

#endif
