/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/static_auto.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_STATIC_AUTO_HPP
#define __LIBLSS_STATIC_AUTO_HPP

#define AUTO_CLASS_NAME(N) RegistratorHelper_##N

#define AUTO_REGISTRATOR_DECL(N) \
namespace LibLSS { \
  namespace StaticInitDummy { \
      struct AUTO_CLASS_NAME(N) { \
         AUTO_CLASS_NAME(N)(); \
      }; \
      \
      static AUTO_CLASS_NAME(N) helper_## N; \
  } \
}

#define AUTO_REGISTRATOR_IMPL(N) LibLSS::StaticInitDummy::AUTO_CLASS_NAME(N)::AUTO_CLASS_NAME(N)() {}


#endif