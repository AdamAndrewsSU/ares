/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/console.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_CONSOLE_HPP
#define __LIBLSS_CONSOLE_HPP

// Log traits automatically available when console is loaded
#include <boost/format.hpp>
#include <boost/chrono.hpp>
#include <iostream>
#include <fstream>
#include <list>
#include <boost/chrono.hpp>
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/tools/log_traits.hpp"
#include "libLSS/tools/static_auto.hpp"
#include "libLSS/tools/function_name.hpp"

namespace LibLSS {

    class Console;

    namespace details {

        class ProgressBase {
        private:
            int numElements;
            int current;
            int percent;
            int iLevel;
            int step;
            std::string msg;
            boost::chrono::system_clock::time_point start;

            friend class Console;

        protected:
            Console *console;

            ProgressBase(Console *c, const std::string& m, int num, int _step);

            virtual ~ProgressBase() {}
        public:
            void destroy();

            virtual void print(const std::string& s) = 0;
            void update(int i);
        };
    };

    template<typename T>
    class Progress: public details::ProgressBase {
    private:
        Progress(Console *c, const std::string& m, int num, int _step)
            : ProgressBase(c, m, num, _step) {}

        friend class Console;
    public:
        virtual void print(const std::string& s);
    };

    class Console;

    namespace timings {
	    void record(std::string const& n, double t);
    }

    namespace details {
      class ConsoleContextBase;

      extern thread_local ConsoleContextBase *currentContext;

      class ConsoleContextBase {
      protected:
        ConsoleContextBase *previousContext;
# ifdef LIBLSS_TIMED_CONTEXT
        boost::chrono::system_clock::time_point start_context;
        std::string ctx_msg;
  	void record_timing(boost::chrono::duration<double> const& timing) {
  	  timings::record(ctx_msg, timing.count());
  	}
#  else
  	void record_timing(boost::chrono::duration<double> const&) {}
#  endif
      public:
#  ifdef LIBLSS_TIMED_CONTEXT
        std::string const& getMessage() const { return ctx_msg; }
#  else
  	std::string const& getMessage() const { static std::string s = ""; return s; }
#  endif
        static inline ConsoleContextBase& current() { return *currentContext; }

        ConsoleContextBase();
        ~ConsoleContextBase();
      };

      template<typename T>
      class ConsoleContext: public ConsoleContextBase {
      private:
          ConsoleContext(ConsoleContext const& ) {}
      public:
          ConsoleContext(const std::string& code_name);
          ~ConsoleContext();

          template<typename Args>
          void print(const Args& args);
          template<typename T2, typename Args>
          void print2(const Args& args);
      };
    }

    using details::ConsoleContext;

    class Console {
    protected:
        typedef std::list<details::ProgressBase *> PList;

        std::ofstream outputFile;
        int verboseLevel;
        int indentLevel;
        std::string indentString;
        PList all_progress;

        friend class details::ProgressBase;

        template<typename T>
        friend class ConsoleContext;

        Console(int verbose = DEFAULT_LOG_LEVEL::verboseLevel, int indent = 0)
            : verboseLevel(verbose), indentLevel(indent) {
        }
    private:
        void cleanProgress(details::ProgressBase *b) {
            PList::iterator i = all_progress.begin();

            while (i != all_progress.end()) {
                if ((*i) == b)
                    i = all_progress.erase(i);
                else
                    ++i;
            }
        }

    public:
        static Console& instance() {
           static Console singleton;
           return singleton;
        }

        void outputToFile(const std::string& fname) {
            outputFile.close();
            outputFile.open(fname.c_str(), std::ofstream::app);
        }

        void indent() {
            setIndentLevel(indentLevel+2);
        }

        void unindent() {
            setIndentLevel(indentLevel-2);
        }

        void setIndentLevel(int indent) {
            indentString = "";
            for (int i = 0; i < indent/2; i++)
              indentString += "| ";
            indentLevel = indent;
        }

        void setVerboseLevel(int verbose) {
            verboseLevel = verbose;
        }

        template<typename T>
        void setVerboseLevel() {
            setVerboseLevel(T::verboseLevel);
        }

        template<typename T>
        bool willPrint() const {
            return verboseLevel >= T::verboseLevel;
        }

        // Fast track debug. Reduced functionality but less performance loss.
        void debug(const std::string& s) {
#ifndef LIBLSS_CONSOLE_NO_DEBUG_SUPPORT
            if (willPrint<LOG_DEBUG>())
                print<LOG_DEBUG>(s);
#endif
        }

        template<typename T>
        void print(const boost::format& fmt)
        {
#ifdef LIBLSS_CONSOLE_NO_DEBUG_SUPPORT
            if (T::verboseLevel >= LOG_DEBUG::verboseLevel)
              return;
#endif

            print<T>(fmt.str());
        }

        template<typename T>
        void print(std::vector<std::string> const& msg_v) {
#ifdef LIBLSS_CONSOLE_NO_DEBUG_SUPPORT
            if (T::verboseLevel >= LOG_DEBUG::verboseLevel)
              return;
#endif
            for (auto const& s: msg_v)
              print<T>(s);
        }

        template<typename T>
        void print(const std::string& msg) {
#ifdef LIBLSS_CONSOLE_NO_DEBUG_SUPPORT
            if (T::verboseLevel >= LOG_DEBUG::verboseLevel)
              return;
#endif
            MPI_Communication *world = MPI_Communication::instance();
            bool notMainRank = ((T::mainRankOnly) && (world->rank() != 0));

            if (outputFile) {
                std::string fullMessage = T::prefix + indentString + msg;
                outputFile << fullMessage << std::endl;
            }

            if (verboseLevel < T::verboseLevel)
                return;


            std::string fullMessage_c = T::prefix_c + indentString + msg;

            if (!T::mainRankOnly) {
              fullMessage_c = boost::str(boost::format("[% 3d/% 3d] %s") % world->rank() % world->size() % fullMessage_c);
            }
            else if (notMainRank)
                return;
            else {
              fullMessage_c = "[---/---] " + fullMessage_c;
            }

            for (int i = 0; i < T::numOutput; i++)
               if (*T::os[i])
                  (*T::os[i]) << fullMessage_c << std::endl;
        }

        template<typename T>
        void print_memory(size_t n) {
            if (n < 1024L)
              print<T>(boost::format("Requesting %ld bytes") % n);
            else if (n < 1024L*1024L)
              print<T>(boost::format("Requesting %lg kbytes") % (n/1024.));
            else if (n < 1024L*1024L*1024L)
              print<T>(boost::format("Requesting %lg Mbytes") % (n/1024./1024.));
            else
              print<T>(boost::format("Requesting %lg Gbytes") % (n/1024./1024./1024.));
        }

        template<typename T>
        Progress<T>& start_progress(const std::string& msg, int numElements, int step = 10) {
            Progress<T> *p = new Progress<T>(this, msg, numElements, step);

            all_progress.push_back(p);
            return *p;
        }

        void c_assert(bool c, const std::string& msg) {
          if (!c) {
            print<LOG_ERROR>(msg);
            MPI_Communication::instance()->abort();
          }
        }
    };


    inline details::ConsoleContextBase::ConsoleContextBase()
    {
      previousContext = currentContext;
      currentContext = this;
    }

    inline details::ConsoleContextBase::~ConsoleContextBase()
    {
      currentContext = previousContext;
    }



    template<typename T>
    details::ConsoleContext<T>::ConsoleContext(const std::string& msg)
      : ConsoleContextBase() {
#ifdef LIBLSS_TIMED_CONTEXT
        start_context = boost::chrono::system_clock::now();
        ctx_msg = msg;
#endif
        Console& c = Console::instance();
        c.print<T>("Entering " + msg);
        c.indent();
    }

    template<typename T>
    details::ConsoleContext<T>::~ConsoleContext()
    {
        Console& c = Console::instance();
        c.unindent();
#ifdef LIBLSS_TIMED_CONTEXT
        boost::chrono::duration<double> ctx_time = boost::chrono::system_clock::now() - start_context;
        c.print<T>(boost::format("Done (in %s) (ctx='%s')") % ctx_time % ctx_msg);
	record_timing(ctx_time);
#else
        c.print<T>("Done");
#endif
    }


    template<typename T>
    template<typename Args>
    void details::ConsoleContext<T>::print(const Args& args) {
#ifdef LIBLSS_CONSOLE_NO_DEBUG_SUPPORT
        if (T::verboseLevel >= LOG_DEBUG::verboseLevel)
            return;
#endif
        Console::instance().print<T>(args);
    }


    template<typename T>
    template<typename T2, typename Args>
    void details::ConsoleContext<T>::print2(const Args& args) {
/*        if (T2::verboseLevel < T::verboseLevel)
            return;*/
#ifdef LIBLSS_CONSOLE_NO_DEBUG_SUPPORT
        if (T::verboseLevel >= LOG_DEBUG::verboseLevel)
            return;
#endif
        Console::instance().print<T2>(args);
    }


    template<typename T>
    void Progress<T>::print(const std::string& m) {
        Console::instance().print<T>(m);
    }

};

AUTO_REGISTRATOR_DECL(console_timing);

#define LIBLSS_AUTO_CONTEXT(level,name) LibLSS::ConsoleContext<level> name(std::string("[" __FILE__ "]") + LIBLSS_FUNCTION)

#endif
