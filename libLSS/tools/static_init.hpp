/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/static_init.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_STATIC_INIT_HPP
#define __LIBLSS_STATIC_INIT_HPP

#include <vector>
#include <queue>
#include <boost/function.hpp>
#include <string>
#include "libLSS/tools/console.hpp"
#include "libLSS/tools/log_traits.hpp"

namespace LibLSS {

    class RegisterStaticInitBase {
    protected:
        int priority;
        std::string text;
        friend struct CompareStaticInit;
        friend struct CompareStaticFinal;
        friend class StaticInit;
    public:
        virtual void executeStaticInit() = 0;
        virtual void executeStaticFinal() = 0;
        virtual ~RegisterStaticInitBase() {}
    };
    
    struct CompareStaticInit {
        bool operator()(RegisterStaticInitBase *a, RegisterStaticInitBase *b) {
            return a->priority >= b->priority;
        }
    };

    struct CompareStaticFinal {
        bool operator()(RegisterStaticInitBase *a, RegisterStaticInitBase *b) {
            return a->priority <= b->priority;
        }
    };

    class StaticInit {
    private:
        StaticInit() {}

        void _execute() {
            while (!all_initializers.empty()) {
                RegisterStaticInitBase *i = all_initializers.top();
                if (i->text.length() > 0) {
                    Console::instance().print<LOG_DEBUG>("INIT: " + i->text);
                }
                i->executeStaticInit();
                all_initializers.pop();
            }
        }

        void _finalize() {
            while (!all_finalizers.empty()) {
                RegisterStaticInitBase *i = all_finalizers.top();
                if (i->text.length() > 0) {
                    Console::instance().print<LOG_DEBUG>("CLEANUP: " + i->text);
                }
                i->executeStaticFinal();
                all_finalizers.pop();
            }
        }

    public:
        static const int MAX_PRIORITY = 0;
        static const int MIN_PRIORITY = 99;
        typedef std::priority_queue<RegisterStaticInitBase *, std::vector<RegisterStaticInitBase *>, CompareStaticInit> InitList;
        typedef std::priority_queue<RegisterStaticInitBase *, std::vector<RegisterStaticInitBase *>, CompareStaticFinal> FinalList;
        InitList all_initializers;
        FinalList all_finalizers;
        
        static StaticInit& instance();
        
        void add(RegisterStaticInitBase *b) {
            all_initializers.push(b);
            all_finalizers.push(b);
        }
        
        static void execute() {
            instance()._execute();
        }

        static void finalize() {
            instance()._finalize();
        }
 
    };
    
    class RegisterStaticInit: public RegisterStaticInitBase {
    public:
        boost::function0<void> function;
        boost::function0<void> function_final;
        
        template<typename Derived>
        RegisterStaticInit(Derived f, int _priority = StaticInit::MIN_PRIORITY, const std::string& _text = "") {
            function = f;
            this->priority = _priority;
            this->text = _text;
            StaticInit::instance().all_initializers.push(this);
        }

        template<typename Derived1, typename Derived2>
        RegisterStaticInit(Derived1 f, Derived2 f2, int _priority = StaticInit::MIN_PRIORITY, const std::string& _text = "") {
            function = f;
            function_final = f2;
            this->priority = _priority;
            this->text = _text;
            StaticInit::instance().all_initializers.push(this);
            StaticInit::instance().all_finalizers.push(this);
        }

        virtual void executeStaticInit() {
            function();
        }
 
        virtual void executeStaticFinal() {
            function_final();
        }

    private:       
        RegisterStaticInit() {
        }        
    };

};

#endif
