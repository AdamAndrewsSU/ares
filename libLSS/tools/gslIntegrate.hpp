/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/gslIntegrate.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_INTEGRATE_HPP
#define __LIBLSS_INTEGRATE_HPP

#include <gsl/gsl_integration.h>
#include "libLSS/tools/gsl_error.hpp"

namespace LibLSS {

    namespace details {
        template<typename FunT>
        double gslSpecialFunction(double x, void *param)
        {
            const FunT *f = (const FunT *)param;

            return (*f)(x);
        }
    }

    template<typename FunT>
    double gslIntegrate(const FunT& v, double a, double b, double prec, int NPTS = 1024)
    {
        gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
        gsl_function f;
        double result;
        double abserr;

        f.function = &details::gslSpecialFunction<FunT>;
        f.params = (void*)&v;

        gsl_integration_qag(&f, a, b, prec, 0, NPTS, GSL_INTEG_GAUSS61,
                    w, &result, &abserr);

        gsl_integration_workspace_free(w);

        return result;
    }

}

#endif
