/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/allocator_policy.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)
       Minh Nguyen <minh@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_ALLOCATOR_POLICY_HPP
#define __LIBLSS_ALLOCATOR_POLICY_HPP

namespace LibLSS {

    struct DefaultAllocationPolicy {
		static long getIncrement() { return 1024; }
        //static long getIncrement() { return ( 1024 * 1024 ); }
    };

};

#endif
