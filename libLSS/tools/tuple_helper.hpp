/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/tuple_helper.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2017-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_TUPLE_HELPER_HPP
#define __LIBLSS_TUPLE_HELPER_HPP

#include <tuple>

namespace LibLSS {

    template<size_t start, size_t N, typename Tuple>
    struct _tuple_last_helper
    {
        template<typename... Args>
        static inline 
        auto convert(Tuple t, Args&&... args) 
            -> decltype(
                _tuple_last_helper<start,N-1,Tuple>::convert(t, 
                    std::get<start+N-1>(t), 
                    std::forward<Args>(args)...))
        {
            return _tuple_last_helper<start,N-1,Tuple>::convert(t, std::get<start+N-1>(t), std::forward<Args>(args)...);
        }
    };

    template<size_t start, typename Tuple>
    struct _tuple_last_helper<start,0,Tuple>
    {
        template<typename... Args>
        static inline 
        auto convert(Tuple t, Args&&... args) 
            -> decltype( std::make_tuple(std::forward<Args>(args)...) )
        {
            return std::make_tuple(std::forward<Args>(args)...);
        }
    };


    template<size_t start, typename Tuple>
    auto last_of_tuple(Tuple t) 
        -> decltype(_tuple_last_helper<start, std::tuple_size<Tuple>::value-start, Tuple>::convert(t))
    {
        return _tuple_last_helper<start, std::tuple_size<Tuple>::value-start, Tuple>::convert(t);
    }

}

#endif
