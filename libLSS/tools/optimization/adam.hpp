/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/optimization/adam.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)
       Jens Jasche <j.jasche@tum.de> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_ADAM_HPP
#define __LIBLSS_ADAM_HPP

#include <vector>
#include <algorithm>

#include <boost/multi_array.hpp>
#include <boost/bind.hpp>
#include "libLSS/tools/console.hpp"
#include "libLSS/tools/array_tools.hpp"
#include "libLSS/tools/fused_array.hpp"

#include <vector>

namespace LibLSS {

  template<typename Element = double, typename Vector = std::vector<double>>
  struct ADAM {
    typedef Element element;
    typedef Vector vector;

    Element alpha, beta_1, beta_2, epsilon, epsstop;
    size_t T;

    ADAM() {
      alpha = 1e-2;
      beta_1 = 0.9;
      beta_2 = 0.999;
      epsilon = 1e-8;
      epsstop = 1e-6;
      T = 10000; //sets the maximum number of steps
    }

    void setADAMParams(
        Element _alpha, Element _beta1, Element _beta2, Element _epsilon,
        Element _epsstop, size_t _T) {
      alpha = _alpha;
      beta_1 = _beta1;
      beta_2 = _beta2;
      epsilon = _epsilon;
      epsstop = _epsstop;
      T = _T;
    }

    void
    run(std::function<void(Vector &g, Vector const &x)> grad,
        Vector&x) {
      ConsoleContext<LOG_VERBOSE> ctx("ADAM::run");
      Console &cons = Console::instance();
      using std::pow;

      size_t N = x.size();
      Vector g_t(N);
      Vector m_t(N);
      Vector v_t(N);

      size_t t = 0;

      Progress<LOG_INFO_SINGLE> &progress =
          cons.start_progress<LOG_INFO_SINGLE>("ADAM optimization", T, 10);

      grad(g_t, x); //computes the gradient
      Element gn = 0.0;
      for (size_t i = 0; i < N; i++)
        gn += std::norm(g_t[i]); //get gradient norm

      while ((t < T) and (gn > epsstop * epsstop)) {
        gn = 0.0;
        for (size_t i = 0; i < N; i++) {
          m_t[i] = beta_1 * m_t[i] +
                   (1.0 - beta_1) *
                       g_t[i]; //updates the moving averages of the gradient
          v_t[i] =
              beta_2 * v_t[i] +
              (1.0 - beta_2) *
                  (g_t[i] *
                   g_t[i]); //updates the moving averages of the squared gradient

          Element m_cap =
              m_t[i] /
              (1.0 -
               (pow(beta_1, t + 1))); //calculates the bias-corrected estimates
          Element v_cap =
              v_t[i] /
              (1.0 -
               (pow(beta_2, t + 1))); //calculates the bias-corrected estimates

          x[i] -=
              (alpha * (m_cap) /
               (sqrt(v_cap) + epsilon)); //updates the parameters

          gn += std::norm(g_t[i]); //get gradient norm
        }

        grad(g_t, x); //computes the gradient
        t++;
        progress.update(t);
      }
      progress.destroy();
    }
  };

}; // namespace LibLSS

#endif
