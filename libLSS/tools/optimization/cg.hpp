/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/optimization/cg.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)
       Jens Jasche <j.jasche@tum.de> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_CG_HPP
#define __LIBLSS_CG_HPP

#include <vector>
#include <algorithm>

#include <boost/multi_array.hpp>
#include <boost/bind.hpp>
#include "libLSS/tools/console.hpp"
#include "libLSS/tools/array_tools.hpp"
#include "libLSS/tools/fused_array.hpp"

#include <vector>

namespace LibLSS {

  namespace ConjugateGradient {
    namespace details {
      template <typename T>
      T lazy_conj(T const &c) {
        return c;
      }

      template <typename T>
      std::complex<T> lazy_conj(std::complex<T> const &c) {
        return std::conj(c);
      }
    } // namespace details

    template <typename Element = double, typename Vector = std::vector<Element>>
    class CG {
    public:
      unsigned int cg_refresh;
      double epsilon;
      unsigned int T;
      typedef Element element;
      typedef Vector vector;

    public:
      CG() {
        cg_refresh = 50; //sets refreshing rate for conjugating directions
        epsilon = 1e-8;  // sets convergence criterion
        T = 1000;        // sets maximum number of cg steps
      }

      //pass matrix application operator
      void
      run(std::function<void(Vector &out, const Vector &in)> A, Vector const &b,
          Vector &x) {
        ConsoleContext<LOG_VERBOSE> ctx("CG::run");
        Console &cons = Console::instance();
        size_t N = b.size();
        Vector r(N, 0.0);
        Vector d(N, 0.0);
        Vector q(N, 0.0);

        Element dnew = 0.0;
        Element dold = 0.0;

        //apply matrix to x vector
        A(q, x);

        //initialize values
        for (size_t i = 0; i < N; i++) {
          r[i] = b[i] - q[i];
          d[i] = r[i];

          dnew += std::norm(r[i]);
        }

        dold = dnew;

        int t = 0;

        Progress<LOG_INFO_SINGLE> &progress =
            cons.start_progress<LOG_INFO_SINGLE>(
                "applying conjugate gradient", T, 10);

        while ((t < T) and (dnew > epsilon * dold)) {
          //apply matrix to d vector
          A(q, d);

          Element dq = 0.0;
          for (size_t i = 0; i < N; i++)
            dq += details::lazy_conj(d[i]) * q[i];

          //now determine alpha
          Element alpha = dnew / dq;

          //now update x vector
          for (size_t i = 0; i < N; i++) {
            x[i] += alpha * d[i];
          }

          if (t % cg_refresh == 0) {
            A(q, x);
            for (size_t i = 0; i < N; i++) {
              r[i] = b[i] - q[i];
            }
          } else {
            for (size_t i = 0; i < N; i++) {
              r[i] -= alpha * q[i];
            }
          }

          dnew = 0.0;
          for (size_t i = 0; i < N; i++)
            dnew += std::norm(r[i]);

          Element beta = dnew / dold;

          for (size_t i = 0; i < N; i++) {
            d[i] = r[i] + beta * d[i];
          }

          dold = dnew;

          t++;
          progress.update(t);
        }
        progress.destroy();
      }
    };

  } // namespace ConjugateGradient

  using ConjugateGradient::CG;

}; // namespace LibLSS

#endif
