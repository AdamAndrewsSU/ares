/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/errors.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_TOOLS_ERRORS_HPP
#define __LIBLSS_TOOLS_ERRORS_HPP

#include "libLSS/tools/console.hpp"
#include <exception>
#include <string>

namespace LibLSS {

    class ErrorBase: virtual public std::exception {
    private:
        std::string message;
        ErrorBase() {}
    public:
        ErrorBase(const std::string& m) : message(m) {}
        ErrorBase(const boost::format& m) : message(m.str()) {}
        virtual ~ErrorBase() throw () {}
        
        virtual const char *what() const throw() { return message.c_str(); }
    };

#define LIBLSS_NEW_ERROR(TNAME) \
    class TNAME: virtual public ErrorBase { \
    public: \
        TNAME(const std::string& m): ErrorBase(m) {} \
        TNAME(const boost::format& m): ErrorBase(m) {} \
        virtual ~TNAME() throw () {} \
    };

    LIBLSS_NEW_ERROR(ErrorIO);
    LIBLSS_NEW_ERROR(ErrorBadState);
    LIBLSS_NEW_ERROR(ErrorMemory);
    LIBLSS_NEW_ERROR(ErrorParams);
    LIBLSS_NEW_ERROR(ErrorBadCast);
    LIBLSS_NEW_ERROR(ErrorNotImplemented);
    LIBLSS_NEW_ERROR(ErrorGSL);
    LIBLSS_NEW_ERROR(ErrorLoadBalance);

    template<typename Error>
    void error_helper(const std::string& msg) {
        Console::instance().print<LOG_ERROR>(msg);
        throw Error(msg);
    }

    template<typename Error>
    void error_helper(const boost::format& msg) {
        Console::instance().print<LOG_ERROR>(msg.str());
        throw Error(msg);
    }

};

#endif
