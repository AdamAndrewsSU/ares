/*+
    ARES/HADES/BORG Package -- -- ./libLSS/mpi/real_mpi/mpi_type_translator.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2014-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/

#ifndef LIBLSS_MPI_TYPE_TRANSLATOR_HPP_INCLUDED
#define LIBLSS_MPI_TYPE_TRANSLATOR_HPP_INCLUDED

#include <complex>
#include <iostream>
#include <cstdlib>

namespace LibLSS
{
  template<typename T>
  MPI_Datatype translateMPIType();

#define MPI_FORCE_TYPE(T, val) \
  template<> \
  inline MPI_Datatype translateMPIType<T>() \
  { \
    return val; \
  }

#define MPI_FORCE_COMPOUND_TYPE(T) \
  template<> \
  inline MPI_Datatype translateMPIType<T>() \
  { \
     return MPI_CompoundType<T>::instance().datatype; \
  }

  MPI_FORCE_TYPE(int, MPI_INT);
  MPI_FORCE_TYPE(double, MPI_DOUBLE);
  MPI_FORCE_TYPE(float, MPI_FLOAT);
  MPI_FORCE_TYPE(long, MPI_LONG);
  MPI_FORCE_TYPE(bool, MPI_INT);
  MPI_FORCE_TYPE(unsigned long, MPI_LONG);
  MPI_FORCE_TYPE(unsigned long long, MPI_LONG_LONG_INT);

  struct MPI_GenericCompoundType {
    MPI_Datatype datatype;
    ~MPI_GenericCompoundType() {
  // FIXME: See how to properly free the type before MPI_Finalize
  //    MPI_Type_free(&datatype);
    }
  };

  template<typename T>
  struct MPI_CompoundType {};

  template<typename T> struct MPI_CompoundType<std::complex<T> >: MPI_GenericCompoundType {
      static MPI_CompoundType<std::complex<T> >& instance() {
          static MPI_CompoundType<std::complex<T> > variable;
          return variable;
      }

      MPI_CompoundType<std::complex<T> >() {
          (std::cerr << "Creating complex type " << std::endl).flush();
          int ret = MPI_Type_contiguous(2, translateMPIType<T>(), &datatype);

          if (ret != MPI_SUCCESS) {
            (std::cerr << "Error while creating types for complexes. Code was " << ret << std::endl).flush();
            ::abort();
          }
          MPI_Type_commit(&datatype);
      }
  };

  MPI_FORCE_COMPOUND_TYPE(std::complex<float>);
  MPI_FORCE_COMPOUND_TYPE(std::complex<double>);

#undef MPI_FORCE_TYPE


  template<typename BaseType, size_t Dim>
  struct mpiVectorType {
    typedef mpiVectorType<BaseType, Dim> Self;
    MPI_Datatype datatype;

    inline MPI_Datatype type() const { return datatype; }

    static Self& instance() {
      static Self variable;
      return variable;
    }

    mpiVectorType() {
      int ret = MPI_Type_contiguous(Dim, translateMPIType<BaseType>(), &datatype);

      if (ret != MPI_SUCCESS) {
        ::abort();
      }
      MPI_Type_commit(&datatype);
    }
  };

};

#endif // MPI_TYPE_TRANSLATOR_HPP_INCLUDED
