/*+
    ARES/HADES/BORG Package -- -- ./libLSS/samplers/core/markov.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_MARKOV_SAMPLER_HPP
#define __LIBLSS_MARKOV_SAMPLER_HPP

#include "libLSS/mcmc/global_state.hpp"

namespace LibLSS {

    class MarkovSampler {
    protected:
        virtual void initialize(MarkovState& state) = 0;
        virtual void restore(MarkovState& state) = 0;
    private:
        bool yet_init;
    public:
        MarkovSampler() : yet_init(false) {}
        virtual ~MarkovSampler() {}

        virtual void sample(MarkovState& state) = 0;
        
        void init_markov(MarkovState& state) {
            if (!yet_init) {
                yet_init = true;
                initialize(state);
            }
        }

        void restore_markov(MarkovState& state) {
            if (!yet_init) {
                yet_init = true;
                restore(state);
            }
        }

        
    };

}

#endif
