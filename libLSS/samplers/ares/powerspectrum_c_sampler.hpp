/*+
    ARES/HADES/BORG Package -- -- ./libLSS/samplers/ares/powerspectrum_c_sampler.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_POWERSPECTRUM_C_SAMPLER_HPP
#define __LIBLSS_POWERSPECTRUM_C_SAMPLER_HPP

#include <iostream>
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/samplers/core/markov.hpp"
#include "libLSS/samplers/core/types_samplers.hpp"
#include "libLSS/samplers/core/powerspec_tools.hpp"

namespace LibLSS {

    class PowerSpectrumSampler_c: public PowerSpectrumSampler_Coloring {
    protected:
        typedef boost::multi_array_ref< IArrayType::ArrayType::element, 1> FlatIntType;

        long localNtot;
        int total_accepted, total_tried;

        bool init_sampler;
        IArrayType1d *counter_evaluations;
        ArrayType1d *sigma_init;

        void base_init(MarkovState& state);
        
        double log_likelihood(MarkovState& state, int k, double P_trial);
        
    public:
        PowerSpectrumSampler_c(MPI_Communication *comm);
        virtual ~PowerSpectrumSampler_c();

        virtual void restore(MarkovState& state);
        virtual void initialize(MarkovState& state);
        virtual void sample(MarkovState& state);    
    };

}

#endif
