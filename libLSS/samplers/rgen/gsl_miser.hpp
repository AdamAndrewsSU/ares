/*+
    ARES/HADES/BORG Package -- -- ./libLSS/samplers/rgen/gsl_miser.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2017-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __GSL_RANDOM_NUMBER_MISER_HPP
#define __GSL_RANDOM_NUMBER_MISER_HPP

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_miser.h>
#include <cstring>
#include "libLSS/tools/errors.hpp"
#include "libLSS/samplers/core/random_number.hpp"
#include "libLSS/samplers/rgen/gsl_random_number.hpp"

namespace LibLSS {

  class GSL_Miser {
  protected:
    gsl_monte_miser_state *state;
    size_t Nd;

    template<typename Functor>
    struct MiserCall {
      Functor f;
      
      MiserCall(Functor g) : f(g) {}
    };

    template<typename Functor>
    static double adaptor_functor(double *x, size_t, void *params)
    {
      MiserCall<Functor> *c = (MiserCall<Functor> *) params;
      
      return c->f(x);
    }
  
  public:
    GSL_Miser(size_t dim) 
      : state(0), Nd(dim) {
      state = gsl_monte_miser_alloc(dim);
    }
    
    ~GSL_Miser() {
      gsl_monte_miser_free(state);
    }
    
    // Only valid for GSL
    template<typename Functor,typename A>
    double integrate(GSL_RandomNumber& rng, Functor f, A& xl, A& xu, size_t calls, double &abserr) {
      gsl_monte_function mf;
      MiserCall<Functor> call(f);
      double result;
      int err;

      mf.f = &adaptor_functor<Functor>;
      mf.dim = Nd;
      mf.params = &call;
      
      if ((err = gsl_monte_miser_integrate(&mf, &xl[0], &xu[0], Nd, calls, rng.rng, state, &result, &abserr)) != GSL_SUCCESS)
        error_helper<ErrorGSL>(boost::format("Error while doing monte carlo integration: error code = %d ") % err);
      return result;
    }
    
    
    template<typename Rng, typename Functor, typename A>
    double integrate(RandomNumberThreaded<Rng>& rng, Functor f, A& xl, A& xu, size_t calls, double &abserr) {
      return integrate(rng.base(), f, xl, xu, calls, abserr);
    }
  };

}

#endif
