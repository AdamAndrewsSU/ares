/*+
    ARES/HADES/BORG Package -- -- ./libLSS/data/postools.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)
       Minh Nguyen <minh@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_POSTOOLS_HPP
#define __LIBLSS_POSTOOLS_HPP

#include <cmath>

namespace LibLSS {

    template<typename T, typename Array>
    void loadPosition(T x, T y, T z, Array& xyz) {
        xyz[0] = x;
        xyz[1] = y;
        xyz[2] = z;
    }

    template<typename T, typename Array>
    void loadVelocity(T vx, T vy, T vz, Array& vxyz) {
		vxyz[0] = vx;
		vxyz[1] = vy;
		vxyz[2] = vz;
    }

     //template<typename T, typename Array>
    //void ComputeRedshiftSpacePosition(Array& xyz, Array& vxyz) {
    //}

};

#endif
