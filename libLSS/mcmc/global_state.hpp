/*+
    ARES/HADES/BORG Package -- -- ./libLSS/mcmc/global_state.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Franz Elsner <felsner@nca-08.MPA-Garching.MPG.DE> (2017)
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2014-2018)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef _GLOBAL_STATE_HPP
#define _GLOBAL_STATE_HPP

#include <boost/type_traits/is_base_of.hpp>
#include <boost/format.hpp>
#include <functional>
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/tools/console.hpp"
#include "libLSS/mcmc/state_element.hpp"

namespace LibLSS
{

    class MarkovState {
      public:
          typedef std::map<std::string, bool> SaveMap;
          typedef std::map<std::string, StateElement *> StateMap;
      private:
         SaveMap save_map;
         StateMap state_map;
      public:
         MarkovState(MarkovState const& ) = delete;
        MarkovState() {}
        ~MarkovState() {
            for (StateMap::iterator i = state_map.begin(); i != state_map.end(); ++i) {
                Console::instance().print<LOG_VERBOSE>(boost::format("Destroying %s") % i->first);
                delete i->second;
            }
            save_map.clear();
        }

        template<typename T>
        static void check_class() {
            BOOST_MPL_ASSERT_MSG((boost::is_base_of<StateElement,T>::value), T_is_not_a_StateElement, ());
        }

        /*
         * This function makes a lookup and a dynamic cast to the specified template "StateElement".
         * It tries to find the indicated state element by name. If it fails an error is thrown.
         * A dynamic cast is then issued to ensure that the stored type is the same as the requested one.
         */
        template<typename T>
        T *get(const std::string& name) {
            check_class<T>();
            StateMap::iterator i = state_map.find(name);
            if (i == state_map.end() || i->second == 0) {
                error_helper<ErrorBadState>(boost::format("Invalid access to %s") % name);
            }
            T *ptr = dynamic_cast<T *>(i->second);
            if (ptr == 0) {
                error_helper<ErrorBadCast>(boost::format("Bad cast in access to %s") % name);
            }
            return ptr;
        }

        template<typename T>
        T *get(const boost::format& f) { return get<T>(f.str()); }

        template<typename T>
        const T *get(const boost::format& f) const { return get<T>(f.str()); }

        template<typename T>
        const T *get(const std::string& name) const {
            check_class<T>();
            StateMap::const_iterator i = state_map.find(name);
            if (i == state_map.end() || i->second == 0) {
                error_helper<ErrorBadState>(boost::format("Invalid access to %s") % name);
            }

            const T *ptr = dynamic_cast<const T *>(i->second);
            if (ptr == 0) {
                error_helper<ErrorBadCast>(boost::format("Bad cast in access to %s") % name);
            }
            return ptr;
        }

        bool exists(const std::string& name) const {
            return state_map.find(name) != state_map.end();
        }

        StateElement& operator[](const std::string& name) { return *get<StateElement>(name); }
        const StateElement& operator[](const std::string& name) const { return *get<StateElement>(name); }

        StateElement *newElement(const std::string& name, StateElement *elt, const bool& write_to_snapshot = false) {
            state_map[name] = elt;
            elt->name = name;
            set_save_in_snapshot(name, write_to_snapshot);
            return elt;
        }

        StateElement *newElement(const boost::format& f, StateElement *elt, const bool& write_to_snapshot = false) {
            state_map[f.str()] = elt;
            elt->name = f.str();
            set_save_in_snapshot(f, write_to_snapshot);
            return elt;
        }
        // use this instead?
        // StateElement *newElement(const boost::format& name, StateElement *elt, const bool& write_to_snapshot = false) {
        //     return this->newElement(name.str(), elt, write_to_snapshot);
        // }

        template<typename Scalar, size_t N, typename ScalarArray>
        void getSyncScalarArray(const std::string& prefix, ScalarArray&& scalars) {
          for (unsigned int i = 0; i < N; i++) {
            scalars[i] = getSyncScalar<Scalar>(prefix + std::to_string(i));
          }
        }

        template<typename Scalar>
        Scalar& getSyncScalar(const std::string& name) {
            return this->template get<SyncableScalarStateElement<Scalar> >(name)->value;
        }

        template<typename Scalar>
        Scalar& getSyncScalar(const boost::format& name) {
            return this->template getSyncScalar<Scalar>(name.str());
        }

        template<typename Scalar>
        Scalar& getScalar(const std::string& name) {
            return this->template get<ScalarStateElement<Scalar> >(name)->value;
        }

        template<typename Scalar>
        Scalar& getScalar(const boost::format& name) {
            return this->template getScalar<Scalar>(name.str());
        }

        template<typename Scalar>
        ScalarStateElement<Scalar> *newScalar(const std::string& name, Scalar x, const bool& write_to_snapshot = false) {
            ScalarStateElement<Scalar> *elt = new ScalarStateElement<Scalar>();

            elt->value = x;
            newElement(name, elt, write_to_snapshot);
            return elt;
        }

        template<typename Scalar>
        ScalarStateElement<Scalar> *newScalar(const boost::format& name, Scalar x, const bool& write_to_snapshot = false) {
            return this->newScalar(name.str(), x, write_to_snapshot);
        }

        template<typename Scalar>
        SyncableScalarStateElement<Scalar> *newSyScalar(const std::string& name, Scalar x, const bool& write_to_snapshot = false) {
            SyncableScalarStateElement<Scalar> *elt = new SyncableScalarStateElement<Scalar>();

            elt->value = x;
            newElement(name, elt, write_to_snapshot);
            return elt;
        }

        template<typename Scalar>
        SyncableScalarStateElement<Scalar> *newSyScalar(const boost::format& name, Scalar x, const bool& write_to_snapshot = false) {
            return this->newSyScalar(name.str(), x, write_to_snapshot);
        }


        void mpiSync(MPI_Communication& comm, int root = 0) {
	    namespace ph = std::placeholders;
            for (StateMap::iterator i = state_map.begin(); i != state_map.end(); ++i) {
                i->second->syncData(
                    std::bind(&MPI_Communication::broadcast, comm, ph::_1, ph::_2, ph::_3, root)
                );
            }
        }

        void set_save_in_snapshot(const std::string& name, const bool save) {
            save_map[name] = save;
        }

        void set_save_in_snapshot(const boost::format& name, const bool save) {
            set_save_in_snapshot(name.str(), save);
        }

        bool get_save_in_snapshot(const std::string& name) {
            SaveMap::const_iterator i = save_map.find(name);
            if (i == save_map.end()) {
                error_helper<ErrorBadState>(boost::format("Invalid access to %s") % name);
            }
            return i->second;
        }

        bool get_save_in_snapshot(const boost::format& name) {
            return get_save_in_snapshot(name.str());
        }

        void saveState(H5::CommonFG& fg) {
            ConsoleContext<LOG_DEBUG> ctx("saveState");
            H5::Group g_scalar = fg.createGroup("scalars");
            for (auto && i : state_map) {
                ctx.print("Saving " + i.first);
                if (i.second->isScalar())
                    i.second->saveTo(g_scalar);
                else {
                    H5::Group g = fg.createGroup(i.first);
                    i.second->saveTo(g);
                }
            }
        }

        void mpiSaveState(H5::CommonFG& fg, MPI_Communication* comm, bool reassembly, const bool write_snapshot=false) {
            ConsoleContext<LOG_VERBOSE> ctx("mpiSaveState");
            H5::Group g_scalar = fg.createGroup("scalars");

            for (auto&& i : state_map) {
                if (write_snapshot && (! get_save_in_snapshot(i.first))) {
                    ctx.print("Skip saving " + i.first);
                    continue;
                }
                ctx.print("Saving " + i.first);
                if (i.second->isScalar())
                    i.second->saveTo(g_scalar, comm, reassembly);
                else {
                    H5::Group g = fg.createGroup(i.first);
                    i.second->saveTo(g, comm, reassembly);
                }
            }
        }

        void restoreStateWithFailure(H5::CommonFG& fg) {
            Console& cons = Console::instance();
            H5::Group g_scalar = fg.openGroup("scalars");
            for (StateMap::iterator i = state_map.begin(); i != state_map.end(); ++i) {
                cons.print<LOG_VERBOSE>("Attempting to restore " + i->first);
#if H5_VERSION_GE(1,10,1)
                if (!g_scalar.nameExists(i->first)) {
                  cons.print<LOG_WARNING>("Failure to restore");
                  continue; 
                }
#endif
                if (i->second->isScalar())
                    // Partial is only valid for 'scalar' types.
                    i->second->loadFrom(g_scalar, false);
                else {
                    H5::Group g = fg.openGroup(i->first);
                    i->second->loadFrom(g);
                }
             }
        }

        void restoreState(H5::CommonFG& fg, bool partial = false, bool loadSnapshot = false) {
            Console& cons = Console::instance();
            H5::Group g_scalar = fg.openGroup("scalars");

            for (StateMap::iterator i = state_map.begin(); i != state_map.end(); ++i) {
                if (loadSnapshot && !get_save_in_snapshot(i->first))
                  continue;

                cons.print<LOG_VERBOSE>("Restoring " + i->first);
                if (i->second->isScalar())
                    // Partial is only valid for 'scalar' types.
                    i->second->loadFrom(g_scalar, partial);
                else {
                    H5::Group g = fg.openGroup(i->first);
                    i->second->loadFrom(g);
                }
            }
        }

	};

};

#endif
