/*+
    ARES/HADES/BORG Package -- -- ./libLSS/mcmc/state_sync.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_STATE_ELEMENT_SYNC_HPP
#define __LIBLSS_STATE_ELEMENT_SYNC_HPP

#include <functional>
#include "libLSS/tools/console.hpp"
#include "libLSS/mpi/generic_mpi.hpp"

namespace LibLSS {

    class StateElement;
    
    class MPI_SyncBundle {
    protected:
        typedef std::list<StateElement *> List;
        
        List list;
    public:
        MPI_SyncBundle() {}
        ~MPI_SyncBundle() {}
        
        MPI_SyncBundle& operator+=(StateElement *e) {
            list.push_back(e);
            return *this;
        }

        template<typename Function>
        void syncData(Function f) {
            ConsoleContext<LOG_DEBUG> ctx("sync bundle");
            for (List::iterator i = list.begin(); i != list.end(); ++i)
                (*i)->syncData(f);
        }
        
        void mpiBroadcast(MPI_Communication& comm, int root = 0) {
	    namespace ph = std::placeholders;
            syncData(std::bind(&MPI_Communication::broadcast, comm, ph::_1, ph::_2, ph::_3, root));
        }

        void mpiAllMax(MPI_Communication& comm) {
	    namespace ph = std::placeholders;
            syncData(std::bind(&MPI_Communication::all_reduce, comm, MPI_IN_PLACE, ph::_1, ph::_2, ph::_3, MPI_MAX));
        }

        void mpiAllSum(MPI_Communication& comm) {
	    namespace ph = std::placeholders;
            syncData(std::bind(&MPI_Communication::all_reduce, comm, MPI_IN_PLACE, ph::_1, ph::_2, ph::_3, MPI_SUM));
        }
        
    };
    
};

#endif
