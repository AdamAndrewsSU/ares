/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tests/test_schechter.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#include <iostream>
#include <boost/format.hpp>
#include "libLSS/tools/static_init.hpp"
#include "libLSS/tools/console.hpp"
#include "libLSS/physics/cosmo.hpp"
#include "libLSS/data/schechter_completeness.hpp"

using std::cout;
using std::endl;
using namespace LibLSS;

int main()
{
    StaticInit::execute();
    Console::instance().setVerboseLevel<LOG_STD>();
    CosmologicalParameters cosmo_params = {
        0, // omega_k 
        0, // omega_r
        0.30, //omega_m
        0.045, //omega_b
        0.70, //omega_q
        -1, //w
        0.97, //n_s
        0, //wprime        
        0.8, //sigma8
        0, //rsmooth
        0.68, //h
        0, //beta
        0, //z0
        1 //a0
    };
    Cosmology cosmo(cosmo_params);
    GalaxySampleSelection selection;
    SchechterParameters params;
    
    params.Mstar = -23.17;
    params.alpha = -0.9;
    selection.bright_apparent_magnitude_cut = -100;
    selection.faint_apparent_magnitude_cut = 11.5;
    selection.bright_absolute_magnitude_cut = -26;
    selection.faint_absolute_magnitude_cut = -20;

    double zlist[] = { 0.001,0.005, 0.01, 0.02, 0.03};
    double E[] = { 1, 0.929577, 0.455884, 0.0966858, 0.013993}; 
    
    for (int i = 0; i < sizeof(zlist)/sizeof(zlist[0]); i++) {
        double d_comoving;
        d_comoving = cosmo.a2com(cosmo.z2a(zlist[i]));
    
        cout << "C = " << details::computeSchechterCompleteness(cosmo, zlist[i], d_comoving, 
                                          selection,
                                          params) << " expect = " << E[i] << endl;
    }                                  
    return 0;
}