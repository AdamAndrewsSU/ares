/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tests/test_stl_container.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#include <iostream>
#include <vector>
#include <set>
#include "libLSS/tools/is_stl_container.hpp"

using namespace LibLSS;

int main()
{
  //std::cout << is_stl_container_like<std::vector<double>>::value << std::endl;
  std::cout << is_stl_container_like<std::set<double>>::value << std::endl;
  return 0;
}
