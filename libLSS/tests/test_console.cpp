/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tests/test_console.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#include <iostream>
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/tools/static_init.hpp"
#include "libLSS/tools/console.hpp"

using namespace std;
using LibLSS::LOG_STD;
using LibLSS::LOG_WARNING;
using LibLSS::LOG_ERROR;
using boost::format;

static void funInit()
{
    cout << "Dummy static init test" << endl;
}

LibLSS::RegisterStaticInit test_reg(funInit);

int main(int argc, char **argv)
{
    LibLSS::MPI_Communication *mpi_world = LibLSS::setupMPI(argc, argv);

    LibLSS::StaticInit::execute();
    LibLSS::Console& console = LibLSS::Console::instance();

    console.print<LOG_STD>("Test console");
    console.print<LOG_WARNING>("Test warning console");
    console.print<LOG_ERROR>("Test error console");

    LibLSS::Progress<LOG_STD>& p = console.start_progress<LOG_STD>("Test progress", 10);

    console.indent();
    console.print<LOG_STD>("test indent");

    for (int j = 0; j < 10; j++)
    {
        p.update(j);
        console.print<LOG_STD>("indented");
        console.indent();
    }
    p.destroy();
    
    console.print<LOG_STD>(format("This is a formatter test %d, %g") % -2 % 4.3);

    {
      LIBLSS_AUTO_CONTEXT(LOG_STD, ctx);
      ctx.print("Now in context");
    }

    LibLSS::StaticInit::finalize();
    return 0;
}
