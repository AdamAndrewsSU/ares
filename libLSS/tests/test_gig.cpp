/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tests/test_gig.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2017-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#include <iostream>
#include "libLSS/samplers/core/random_number.hpp"
#include "libLSS/samplers/rgen/gsl_random_number.hpp"
#include "libLSS/samplers/core/gig_sampler.hpp"

using std::cout;
using std::endl;
using namespace LibLSS;

int main()
{
  double a = 10.;
  double b = 5.;

  double p = 1 - 30.;

  GSL_RandomNumber rgen;

  for (int i = 0; i < 100000; i++) {
    cout << GIG_sampler_3params(a, b, p, rgen) << endl;
  }

  return 0;

}

