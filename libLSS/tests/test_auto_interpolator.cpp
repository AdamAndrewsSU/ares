/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tests/test_auto_interpolator.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#include <iostream>
#include "libLSS/tools/auto_interpolator.hpp"
#include <boost/lambda/lambda.hpp>
#include <CosmoTool/algo.hpp>

using namespace LibLSS;

int main()
{
  using boost::lambda::_1;
  auto a = build_auto_interpolator(CosmoTool::square<double>, 0., 4., 0.1, 0., 16.);

  for (double i = -2; i < 7; i+=0.01)
    std::cout << i << " " << a(i) << " " << (i*i) << std::endl;

  return 0;
}
