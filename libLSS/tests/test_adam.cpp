/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tests/test_adam.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)
       Jens Jasche <j.jasche@tum.de> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#define BOOST_TEST_MODULE adam
#define BOOST_TEST_NO_MAIN
#define BOOST_TEST_ALTERNATIVE_INIT_API
#include <boost/test/included/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include <boost/multi_array.hpp>
#include "libLSS/tools/static_init.hpp"
#include "libLSS/tools/optimization/adam.hpp"
#include <CosmoTool/algo.hpp>
#include <CosmoTool/hdf5_array.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <algorithm>

namespace utf = boost::unit_test;
using namespace LibLSS;
using boost::extents;
using namespace CosmoTool;
using namespace std;

void grad(vector<double> &out, const vector<double> &in) {
  int N = in.size();
  //initialize values
  for (int i = 0; i < N; i++) {
    out[i] = 0;
    for (int j = 0; j < N; j++) {
      //test with simple correlation function
      double Mij = 0.5 * exp(-0.5 * (i - j) * (i - j));
      out[i] += Mij * (in[j] - 3.1415);
    }
  }
}

int main(int argc, char *argv[])
{
  LibLSS::QUIET_CONSOLE_START = true;
  StaticInit::execute();

  int ret = utf::unit_test_main(&init_unit_test, argc, argv);

  StaticInit::finalize();
  return ret;
}


BOOST_AUTO_TEST_CASE(run)
{
  LibLSS::Console &console = LibLSS::Console::instance();

  ADAM<> adam;

  int N = 2000;
  vector<double> x(N, 0.0);

  adam.run(grad, x);

  double max_dev = 0;
  double eps = 0;
  size_t imax = 0;
  for (size_t i = 0; i < x.size(); i++) {
    double diff = std::abs(x[i]-3.1415);
    BOOST_CHECK_CLOSE(x[i], 3.1415, 1e-5);
    if (max_dev < diff) {
      imax = i;
      max_dev = diff;
    }
    eps += std::norm(diff);
  }

  BOOST_TEST_MESSAGE( "Distance between truth and solution  = " << eps); BOOST_TEST_MESSAGE( "Largest deviation  = " << max_dev << " at element imax =" << imax);
}
