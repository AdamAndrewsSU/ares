#+
#   ARES/HADES/BORG Package -- -- ./get-aquila-modules.sh
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
#!/bin/sh

C_DEFAULT=$(echo -e "\033[0m")
C_WHITE=$(echo -e "\033[1m")
C_BG_RED=$(echo -e "\033[41m")
C_BG_GREEN=$(echo -e "\033[42m")

errormsg() {
  msg=$1
  echo -e "${C_BG_RED}${msg}${C_DEFAULT}"
}


print_help()
{
  cat<<EOF
This is the get-aquila-module helper script. It clones and updates the modules
common to the collaboration. By default it uses SSH protocol. We advise using
a SSH key to avoid being asked too much about your password.

--https USER       Use HTTPS protocol instead of SSH 
--pull             Pull all the modules
--help             This information message
--update-copyright Update copyright notices
--update-indent    Update indentation
EOF
}

check_dirty()
{
  if test $force != 0; then
    return
  fi

  d=$1
  r=$(
    cd $d;
    git status --porcelain | grep '^ M'
  )
  if test x"$r" != x""; then
    echo "Module ${d} is not clean."
    exit 1
  fi
}

echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "This script can be run only by Aquila members."
echo "if your bitbucket login is not accredited the next operations will fail."
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

if ! command -v git 2>&1 > /dev/null; then
  echo "Git is required."
  exit 1
fi

operation='clone'
prefix=git@bitbucket.org:
force=0
while test $# -gt 0; do
  key="$1"
  case $key in
  --https)
     user="$2"
     prefix="https://${user}@bitbucket.org/"
     shift
     ;;
  --pull)
     operation='pull'
     ;;
  --purge)
     operation='purge'
     ;;
  --update-copyright)
     operation="update_copyright"
     ;;
  --update-indent)
     operation="update_indent"
     ;;
  --force)
     force=1
     ;;
  -h|--h|--he|--hel|--help)
     print_help
     exit 0
     ;;
  esac
  shift
done

export prefix
submodules="ares_fg,ares_fg ares_hades,hades ares_borg,borg hmclet,hmclet dm_sheet,dm_sheet" 

case $operation in
pull)
   for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      echo "Pulling ${sub_git} into extra/${sub}"
      (cd extra/$sub; git pull)
   done
   ;;
purge)
   for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      (
        cd extra/${sub};
        if [ -z "$(git status --porcelain)" ]; then
          # Working directory clean
	  cd ..
          echo "Purging ${sub}"
	  rm -fr ${sub}
        else
          echo "Not empty. Skipping"
        fi
      )
   done
   ;;
clone)
  for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      echo "Cloning ${sub_git} into extra/${sub}"
      (
        cd extra/
        if ! test -e ${sub}; then
          git clone ${prefix}bayesian_lss_team/${sub_git}.git ${sub}
        fi
      )
  done
  ;;
update_copyright)
  check_dirty .
  python3 build_tools/gather_sources.py
  for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      echo "Updating ${sub}"
      check_dirty extra/${sub}
      (cd extra/${sub};  python3 ../../build_tools/gather_sources.py)
  done
  ;;
update_indent)
  if ! command -v clang-format; then
    errormsg "Could not find clang-format in the PATH. It is required for reindentation."
    exit 1  
  fi
  CF=$(which clang-format)
  for i in hmclet dm_sheet virbius; do
      if test -d extra/${i}; then
        find extra/${i} -name "*.[ch]pp" | xargs ${CF} -style=file -i  
      fi
  done
esac
