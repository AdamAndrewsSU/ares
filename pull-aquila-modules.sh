#!/bin/sh
#+
#+

C_DEFAULT=$(echo -e "\033[0m")
C_WHITE=$(echo -e "\033[1m")
C_BG_RED=$(echo -e "\033[41m")
C_BG_GREEN=$(echo -e "\033[42m")

errormsg() {
  msg=$1
  echo -e "${C_BG_RED}${msg}${C_DEFAULT}"
}


print_help()
{
  cat<<EOF
This is the pull-aquila-modules helper script. It pulls the modules
common to the collaboration. If required, it pushes the modules to remote 'origin'

--remote REMOTE    Name of the remote in git config file (default 'origin')
--push             Push all the modules to remote 'origin'
--help             This information message
EOF
}

ARES_DIRECTORY=$(cd `dirname $0` && pwd)
REMOTE_NAME="origin"
push=0
while test $# -gt 0; do
  key="$1"
  case $key in
  --remote)
     REMOTE_NAME="$2"
     ;;
  --push)
     push=1
     ;;
  -h|--h|--he|--hel|--help)
     print_help
     exit 0
     ;;
  esac
  shift
done

submodules="ares_fg borg borg_constrained_sims borg_opt dm_sheet hades hmclet" 

for i in $submodules; do
    if [ -d "$ARES_DIRECTORY/extra/$i" ]; then
        cd $ARES_DIRECTORY/extra/$i
        git remote | grep $REMOTE_NAME > /dev/null
        TEST_REMOTE=$?
        if [ $TEST_REMOTE -ne 0 ] || [ $REMOTE_NAME == "origin" ]; then
            echo "Pulling $i from $REMOTE_NAME..."
            git pull origin master
            echo ""
        else
            echo "Pulling $i from $REMOTE_NAME..."
            git pull $REMOTE_NAME master
            echo ""
            if [ $push == 1 ]; then
                echo "Pushing $i to origin..."
                git push origin master
                echo ""
            fi
        fi
    fi
done

# ARES TAG: num_authors = 1
# ARES TAG: name(0) = Florent Leclercq
# ARES TAG: email(0) = florent.leclercq@polytechnique.org
# ARES TAG: year(0) = 2018
