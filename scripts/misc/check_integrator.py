#+
#   ARES/HADES/BORG Package -- -- ./scripts/misc/check_integrator.py
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016, 2018)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
import h5py as h5
import numpy as np
import pylab as plt


fig = plt.figure(1)

fig.clf()
ax = fig.add_subplot(111)

f = h5.File("symplectic.h5")
for k in f.keys():
  ax.semilogy(np.abs(f[k]['energy']), label=k)
f.close()

ax.legend()
fig.savefig("symplectic.png")
