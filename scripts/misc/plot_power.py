#+
#   ARES/HADES/BORG Package -- -- ./scripts/misc/plot_power.py
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015, 2018)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
import read_all_h5
from pylab import *

P=[]
n=[]
b=[]
n1=[]
b1=[]
i=0
while True:
  a = \
   read_all_h5.read_all_h5("mcmc_%d.h5" % i)
  try:
    P.append(a.scalars.powerspectrum)
    n.append(a.scalars.galaxy_nmean_0[0])
    b.append(a.scalars.galaxy_bias_0[0])
    n1.append(a.scalars.galaxy_nmean_1[0])
    b1.append(a.scalars.galaxy_bias_1[0])
  except AttributeError:
    break
  i += 1

k = read_all_h5.read_all_h5("info.h5").scalars.k_modes  
P = np.array(P)

f=figure(1)
clf()
loglog(k[:,None].repeat(P.shape[0],axis=1),P.transpose())

f=figure(2)
clf()
plot(n)

