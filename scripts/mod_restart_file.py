#+
#   ARES/HADES/BORG Package -- -- ./scripts/mod_restart_file.py
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2017-2018)
#      Jens Jasche <j.jasche@tum.de> (2016)
#      elsner <f.elsner@mpa-garching.mpg.de> (2017)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
import h5py
import numpy as np


#set chain
chain_path="."

#set reference file
refdir ='/scratch/jasche/panphasia_run_lc/'
fref=refdir+'restart.h5_0'

#set target file
tardir ='/scratch/jasche/panphasia_run_h/'
ftar=tardir+'restart.h5_0'

#read density files from reference file
print fref
dref = h5py.File(fref, "r")

dref_final_density=dref['/scalars/BORG_final_density']
dref_s_field=dref['/scalars/s_field']
dref_s_hat_field=dref['/scalars/s_hat_field']


dtar = h5py.File(ftar, "r+")
dtar_final_density=dtar['/scalars/BORG_final_density']
dtar_s_field=dtar['/scalars/s_field']
dtar_s_hat_field=dtar['/scalars/s_hat_field']

'''
WARNING: At this point you will irretrievably
modify your restart file!!!!!!
'''


dtar_final_density[...] = dref_final_density[...]
dtar_s_field[...]       = dref_s_field[...]
dtar_s_hat_field[...]   = dref_s_hat_field[...]


dref.close()
dtar.close()
