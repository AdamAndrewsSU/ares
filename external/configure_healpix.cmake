IF (NOT EXISTS "${HEALPIX_DIR}/autotools/configure")
    execute_process(
        WORKING_DIRECTORY "${HEALPIX_DIR}/autotools"
        COMMAND autoreconf -i
        RESULT_VARIABLE okcode)
        
    IF (NOT "${okcode}" STREQUAL "0")
      MESSAGE(FATAL_ERROR "Cannot create configure command. Is autoconf/autoreconf installed ?")
    ENDIF (NOT "${okcode}" STREQUAL "0")
        
ENDIF (NOT EXISTS "${HEALPIX_DIR}/autotools/configure")

SET(ENV{CFITSIO_LIBS} "-L${HEALPIX_INSTALL}/lib -lcfitsio")
SET(ENV{CFITSIO_CFLAGS} -I${HEALPIX_INSTALL}/include)

execute_process(
    COMMAND "${HEALPIX_DIR}/autotools/configure" --prefix=${HEALPIX_INSTALL} --disable-shared --with-pic --disable-maintainer-mode CC=${HEALPIX_CC} CXX=${HEALPIX_CXX}
    
    RESULT_VARIABLE okcode
)

IF (NOT "${okcode}" STREQUAL "0")
    MESSAGE(FATAL_ERROR "Cannot execute configure command")
ENDIF (NOT "${okcode}" STREQUAL "0")

